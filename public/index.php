<?php
include_once '../vendor/autoload.php';

use App\Model\User;

/**  Example table users in MySql database

    create table users
    (
        id         int auto_increment
            primary key,
        name       varchar(255) null,
        email      varchar(255) null,
        password   varchar(255) null,
        age        int          null,
        status     int          null,
        created_at datetime     null
    );
**/

// Load data to database from file

//$users = include 'users.php';
//foreach ($users as $user){
//    $u = new User($user);
//    $u->create();
//}

$id = 5;
$user = User::findById($id);
echo 'findById(' . $id. '): <br>';
var_dump($user);


$email = 'logan@gmail.com';
$status = 1;
$user = User::findByEmailAndByStatus($email, $status);
echo "<br><br>findByEmailAndByStatus('$email', '$status'): <br>";
var_dump($user);


$startDate = '2020-01-02';
$endDate = '2020-01-05';
$users = User::findBetweenCreatedAt($startDate, $endDate);
echo "<br><br>findBetweenCreatedAt('$startDate', '$endDate'): <br>";
var_dump($users);


$startDate = '2020-01-05';
$endDate = '2020-01-10';
$status = 2;
$users = User::findBetweenCreatedAtAndByStatus($startDate, $endDate, $status);
echo "<br><br>findBetweenCreatedAtAndByStatus('$startDate', '$endDate', '$status'): <br>";
var_dump($users);


$startDate = '2020-01-10';
$endDate = '2020-01-13';
$status = [1, 2];
$users = User::findBetweenCreatedAtAndInStatus($startDate, $endDate, $status);
echo "<br><br>findBetweenCreatedAtAndInStatus('$startDate', '$endDate', [" . implode(', ', $status) . "]): <br>";
var_dump($users);