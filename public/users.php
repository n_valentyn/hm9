<?php
return [
    ['name' => 'Jon', 'email' => 'jon@gmail.com', 'password' => 'jon12345', 'age' => 18, 'status' => 1, 'created_at' => '2020-01-01 12:01:45'],
    ['name' => 'Emma', 'email' => 'emma@gmail.com', 'password' => 'emma453', 'age' => 19, 'status' => 2, 'created_at' => '2020-01-02 12:01:45'],
    ['name' => 'Olivia', 'email' => 'olivia@gmail.com', 'password' => 'olivia342', 'age' => 20, 'status' => 1, 'created_at' => '2020-01-03 12:01:45'],
    ['name' => 'Ava', 'email' => 'ava@gmail.com', 'password' => 'ava5685432', 'age' => 18, 'status' => 2, 'created_at' => '2020-01-04 12:01:45'],
    ['name' => 'Isabella', 'email' => 'isabella@gmail.com', 'password' => 'isabella34', 'age' => 19, 'status' => 1, 'created_at' => '2020-01-05 12:01:45'],
    ['name' => 'Sophia', 'email' => 'sophia@gmail.com', 'password' => 'sophia12', 'age' => 20, 'status' => 2, 'created_at' => '2020-01-06 12:01:45'],
    ['name' => 'Charlotte', 'email' => 'charlotte@gmail.com', 'password' => 'charlotte89', 'age' => 21, 'status' => 1, 'created_at' => '2020-01-07 12:01:45'],
    ['name' => 'Mia', 'email' => 'mia@gmail.com', 'password' => 'mia532109', 'age' => 17, 'status' => 2, 'created_at' => '2020-01-08 12:01:45'],
    ['name' => 'Liam', 'email' => 'liam@gmail.com', 'password' => 'liam5678675', 'age' => 18, 'status' => 1, 'created_at' => '2020-01-09 12:01:45'],
    ['name' => 'Noah', 'email' => 'noah@gmail.com', 'password' => 'noah234', 'age' => 19, 'status' => 2, 'created_at' => '2020-01-10 12:01:45'],
    ['name' => 'William', 'email' => 'william@gmail.com', 'password' => 'william00', 'age' => 20, 'status' => 1, 'created_at' => '2020-01-11 12:01:45'],
    ['name' => 'James', 'email' => 'james@gmail.com', 'password' => 'james2232', 'age' => 21, 'status' => 2, 'created_at' => '2020-01-12 12:01:45'],
    ['name' => 'Logan', 'email' => 'logan@gmail.com', 'password' => 'logan896', 'age' => 18, 'status' => 1, 'created_at' => '2020-01-13 12:01:45'],
    ['name' => 'Benjamin', 'email' => 'benjamin@gmail.com', 'password' => 'benjamin191', 'age' => 20, 'status' => 2, 'created_at' => '2020-01-014 12:01:45'],
    ['name' => 'Mason', 'email' => 'mason@gmail.com', 'password' => 'mason9196', 'age' => 19, 'status' => 1, 'created_at' => '2020-01-015 12:01:45'],
];