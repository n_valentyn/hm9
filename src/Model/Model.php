<?php

namespace App\Model;

use App\Storage\MySqlStorage;
use App\Storage\StorageInterface;
use App\Helper\Pluralize;

abstract class Model
{
    //What storage to use
    protected static string $default_storage = MySqlStorage::class;

    //Storage object
    protected static StorageInterface $storage;

    //What name for the table will be used in the model
    protected static string $table;

    //Primary table key
    protected static string $primary_key = 'id';

    //Reserved class properties
    protected static array $reserved_properties = [
        'default_storage',
        'storage',
        'table',
        'primary_key',
        'reserved_properties'
    ];

    public function __construct(array $data = [])
    {
        static::initialization();

        foreach ($data as $field => $value) {

            //If the field is present in the class and is not reserved then write it
            if (property_exists(static::class, $field)
                && !in_array($field, self::$reserved_properties)) {

                $this->$field = $value;

            } else {
                // ? throw new \InvalidArgumentException();
            }
        }
    }

    //Implementation of dynamic getters and setters
    public function __call($name, $arguments)
    {
        $modifier = substr($name, 0, 3);
        $fieldName  = lcfirst(substr($name, 3));

        if($modifier === 'set'){
            //If the field is present in the class and is not reserved then set it
            if (property_exists(static::class, $fieldName)
                && !in_array($fieldName, self::$reserved_properties)) {
                $this->$fieldName = $arguments[0];
            }
        }

        if($modifier === 'get'){
            //If the field is present in the class and is not reserved then get it
            if (property_exists(static::class, $fieldName)
                && !in_array($fieldName, self::$reserved_properties)) {
                return $this->$fieldName;
            }
        }
    }

    //Initialization of storage and table
    public static function initialization()
    {
        //If the table name is not set, then we get it from the class name
        if(empty(static::$table)){
            static::$table = Pluralize::pluralize(2, strtolower(end(explode('\\', static::class))));
        }

        //If the storage is not set, then set it to default storage
        if(empty(static::$storage)){
            static::$storage = new self::$default_storage;
        }

    }

    //Get records by id
    public static function find($id)
    {
        static::initialization();

        $data = static::$storage->find(static::$table, $id);

        if($data === false) {
            throw new \Exception('Record does not exist.');
        }

        return new static($data);
    }

    //Creat records
    public function create()
    {
        //If an identifier is set in the object,
        // then we believe that the record has already been created and we throw exceptions
        if(isset($this->{static::$primary_key})){
            throw new \Exception('Primary key "'
                . static::$primary_key
                . '" cannot be filled when creating a record');

            //or
            //unset($data[static::$primary_key]);
        }

        //Create a record
        $this->{static::$primary_key} = static::$storage->create(static::$table, get_object_vars($this));

        return $this;
    }

    //Update records
    public function update()
    {
        static::$storage->update(static::$table, static::$primary_key, get_object_vars($this));

        return $this;
    }

    //Delete records
    public function delete()
    {
        static::$storage->delete(static::$table, $this->id);
    }

    //Manage storage magic methods
    public static function __callStatic($methodName, $arguments)
    {
        $patternFind = '/^find/';

        //If the method name begins "find" then
        if(preg_match($patternFind, $methodName)){
            static::initialization();

            $data = static::$storage->magicFind(static::$table, $methodName, $arguments);

            if(empty($data)) {
                throw new \Exception('Record does not exist.');
            }

            $collection = [];
            foreach ($data as $row){
                $collection[] = new static($row);
            }

            return $collection;
        }

        throw new \Exception('Method ' . $methodName . ' not found!');
    }
}