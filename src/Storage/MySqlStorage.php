<?php


namespace App\Storage;

use App\Storage\MySqlConnection;

class MySqlStorage implements StorageInterface
{
    protected \PDO $connection;

    public function __construct()
    {
        //Set connection
        $this->connection = MySqlConnection::getConnection();
    }

    //Get records from table by id
    public function find(string $table, int $id)
    {
        $sql = 'SELECT * FROM ' . $table . ' WHERE id = :id';
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    //Create records in table from data
    public function create(string $table, array $data)
    {
        $sql = 'INSERT INTO ' . $table;
        $sql .= ' (' . implode(', ', array_keys($data)) . ') ';
        $sql .= 'VALUES (:' . implode(', :', array_keys($data)) . ')';

        $stmt = $this->connection->prepare($sql);

        foreach ($data as $field => $value){
            if($value === null){
                $stmt->bindValue(':' . $field, $value, \PDO::PARAM_NULL);
                continue;
            }
            $stmt->bindValue(':' . $field, $value);
        }

        $stmt->execute();

        return $this->connection->lastInsertId();
    }

    //Update records in table by id from data
    public function update(string $table, string $idName, array $data)
    {
        //Сut the identifier from the data array
        $id = $data[$idName];
        unset($data[$idName]);

        //Preparation fields for sql
        $field_preparation = [];
        foreach (array_keys($data) as $key){
            $field_preparation[] = $key . ' = :' . $key;
        }

        $sql = 'UPDATE ' . $table . ' SET ';
        $sql .= implode(', ', $field_preparation);
        $sql .= ' WHERE ' . $idName . ' = :' . $idName ;

        $stmt = $this->connection->prepare($sql);

        foreach ($data as $field => $value){
            if($value === null){
                $stmt->bindValue(':' . $field, $value, \PDO::PARAM_NULL);
                continue;
            }
            $stmt->bindValue(':' . $field, $value);
        }
        $stmt->bindValue(':' . $idName, $id);

        $stmt->execute();
    }

    //Delete records from table by id
    public function delete(string $table, int $id)
    {
        $sql = 'DELETE FROM ' . $table . ' WHERE id = :id';
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }

    public function magicFind(string $table, string $methodName, array $arguments)
    {
        $patternFind = '/^find/';
        $patternBy = '/^By/';
        $patternBetween = '/^Between/';
        $patternIn = '/^In/';
        $splitAnd = 'And';

        //If the method name does not begins "find" throw exception
        if(!preg_match($patternFind, $methodName)){
            throw new \Exception('Method ' . $methodName . ' not found!');
        }

        //Remove 'find' from the beginning of the method name
        $queryString = preg_replace($patternFind, '', $methodName);

        //make an array with sub lines
        $arraySubLine = explode($splitAnd, $queryString);

        $preparationSql = [];
        $data = [];
        foreach ($arraySubLine as $subLine){

            if(empty($arguments)){
                throw new \Exception('Too few arguments in the method: ' . $methodName . '!');
            }

            //By
            if (preg_match($patternBy, $subLine)) {
                $fieldCamelCase = preg_replace($patternBy, '', $subLine);
                $field = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $fieldCamelCase));
                $preparationSql[] = ' ' . $field . ' = :' . $field;
                $data[$field] = array_shift($arguments);
                continue;
            }

            //Between
            if (preg_match($patternBetween, $subLine)) {
                $fieldCamelCase = preg_replace($patternBetween, '', $subLine);
                $field = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $fieldCamelCase));
                $preparationSql[] = ' ' . $field . ' BETWEEN :' . 'start_' . $field . ' AND :' . 'end_' . $field;
                $data['start_' . $field] = array_shift($arguments);
                $data['end_' . $field] = array_shift($arguments);
                continue;
            }

            //In
            if (preg_match($patternIn, $subLine)) {
                $fieldCamelCase = preg_replace($patternIn, '', $subLine);
                $field = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $fieldCamelCase));

                if(!is_array($arguments[0])){
                    throw new \Exception('The field "' . $field . '" - must be an array!');
                }

                $preparationSql[] = ' ' . $field . ' IN (:' . $field . '_' . implode(', :' . $field . '_', array_keys($arguments[0])) . ')';
                foreach (array_shift($arguments) as $key => $value){
                    $data[$field . '_' . $key] = $value;
                }
                continue;
            }

            throw new \Exception('The find method: ' . $methodName . ' has problem!');
        }



        $sql = 'SELECT * FROM ' . $table . ' WHERE';
        $sql .= implode(' AND', $preparationSql);

        $stmt = $this->connection->prepare($sql);

        foreach ($data as $field => $value){
            if($value === null){
                $stmt->bindValue(':' . $field, $value, \PDO::PARAM_NULL);
                continue;
            }
            $stmt->bindValue(':' . $field, $value);
        }

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}