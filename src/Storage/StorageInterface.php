<?php


namespace App\Storage;


interface StorageInterface
{
    //Get records from table by id
    public function find(string $table, int $id);

    //Create records in table from data
    public function create(string $table, array $data);

    //Update records in table by id from data
    public function update(string $table, string $idName, array $data);

    //Delete records from table by id
    public function delete(string $table, int $id);

    public function magicFind(string $table, string $methodName, array $arguments);
}